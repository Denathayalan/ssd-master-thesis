'''
Plot 3D scatter plot of mean RGB values saved in given csv file
Master thesis project Cybercom
Roger Kalliomaki
'''

# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
import numpy as np
import csv

# Set dataset and csv file path
FILE_PATH = './rgb_values.csv'
DATASET = 'BDD100K'

# Go through csv file and fill in rgb values in the 3D scatter plot.
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
with open(FILE_PATH) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
        else:
            xs = int(row[0])
            ys = int(row[1])
            zs = int(row[2])
            if line_count == 1:
                ax.scatter(xs, ys, zs, c='red', s=1, label=DATASET)
            else:
                ax.scatter(xs, ys, zs, c='red', s=1)
            line_count += 1
ax.set_xlabel('R')
ax.set_ylabel('G')
ax.set_zlabel('B')
ax.set_xlim(-50, 300)
ax.set_ylim(-50, 300)
ax.set_zlim(-50, 300)
plt.legend(loc=2)
plt.savefig('3Dplot2.png')
plt.show()
