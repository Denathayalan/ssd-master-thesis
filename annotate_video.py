'''
Annotate video using SSD predictions
Master thesis project Cybercom
Roger Kalliomaki
'''

import cv2
import numpy as np
from models.keras_ssd300 import ssd_300
from keras import backend as K
from keras.optimizers import Adam
from keras_loss_function.keras_ssd_loss import SSDLoss

# Set file paths, image size, nr of classes, if to use png files, model mode,
# and path to saved model file
IN_FILE_PATH = '<PATH TO ORIGINAL VIDEO>'
OUT_FILE_PATH = '<PATH TO NEW ANNOTATED VIDEO>'
IMG_HEIGHT = 300
IMG_WIDTH = 300
NR_CLASSES = 10
USE_PNG = False
MODEL_MODE = 'inference'
SAVED_MODEL_PATH = '<SAVED MODEL PATH>'

SHORTEN = False
START_SECOND = 3
NR_SECONDS = 57


# The per-channel mean of the images in the dataset
#VOC 2007 [113, 107,  99]
#VOC 2012 [114, 109, 101]
#COCO [123, 117, 104]
#BBD100K [70, 74, 74]
#FCAV [107, 104, 99]
MEAN_COLOR = [70, 74, 74]
SCALES = [0.05, 0.1, 0.29, 0.48, 0.67, 0.86, 1.05]

# Aspect ratios the network was trained with
ASPECT_RATIOS = [[1.0, 1.5, 0.5],
                 [1.0, 1.5, 0.5, 2.0, 2.5],
                 [1.0, 1.5, 0.5, 2.0, 2.5],
                 [1.0, 1.5, 0.5, 2.0, 2.5],
                 [1.0, 1.5, 0.5],
                 [1.0, 1.5, 0.5]]

# Set classes
classes = ['background', 'bus', 'traffic light', 'traffic sign', 'person', 'bike', 'truck', 'motor', 'car', 'train', 'rider']

# Build new model
# Clear previous models from memory
K.clear_session()

# Set parameters
model = ssd_300(image_size=(IMG_HEIGHT, IMG_WIDTH, 3),
                        n_classes=NR_CLASSES,
                        mode=MODEL_MODE,
                        l2_regularization=0.0005,
                        scales=SCALES,
                        aspect_ratios_per_layer=ASPECT_RATIOS,
                        two_boxes_for_ar1=True,
                        steps=[8, 16, 32, 64, 100, 300],
                        offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                        clip_boxes=False,
                        variances=[0.1, 0.1, 0.2, 0.2],
                        normalize_coords=True,
                        subtract_mean=MEAN_COLOR,
                        swap_channels=[2, 1, 0],
                        confidence_thresh=0.5,
                        iou_threshold=0.45,
                        top_k=200,
                        nms_max_output_size=400)

# Load the trained weights into the model
weights_path = SAVED_MODEL_PATH
model.load_weights(weights_path, by_name=True)

# Compile the model so that Keras won't complain the next time you load it
optmzr = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
model.compile(optimizer=optmzr, loss=ssd_loss.compute_loss)

# Define colors for each possible object
colors = {'aeroplane' : (255,0,0), 'bicycle' :  (0,255,0), 'bird' :  (0,0,255), 'boat' :  (255,255,0), 'bottle' :  (0,255,255), 'bus' :  (255,0,255), 'car' :  (0,255,127), 'cat' :  (128,128,0), 'chair' :  (0,128,0), 'cow' :  (128,0,128), 'diningtable' :  (0,0,128), 'dog' :  (178,34,34), 'horse' :  (220,20,60), 'motorbike' :  (205,92,92), 'person' :  (255,165,0), 'pottedplant' : (128,0,0) , 'sheep' :  (0,139,139), 'sofa' :  (138,43,226), 'train' :  (147,112,219),  'tvmonitor' : (219,112,147), 'traffic light' : (112,219,147), 'traffic sign' : (138,226,43), 'bike' :  (0,255,0), 'truck' :  (0,128,0), 'motor' :  (205,92,92), 'rider' :  (230,150,0), 'Car' :  (128,0,0)}

# Settings for relative sizes depending on image size and loading first frame
font_scale = 1.5
font = cv2.FONT_HERSHEY_PLAIN
cap = cv2.VideoCapture(IN_FILE_PATH)
fps = int(cap.get(cv2.CAP_PROP_FPS))
nr_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
nr_seconds = nr_frames/fps
fourcc   = cv2.VideoWriter_fourcc(*'MJPG')
ret, frame = cap.read()
orig_width = frame.shape[1]
orig_height = frame.shape[0]
width_scale = float(13*orig_width)/1280.0
height_scale = float(orig_height)/720.0
text_box_width = 0
text_box_height = int(25.0*height_scale)
text_pos_height = int(20.0*height_scale)
font_scale = font_scale*height_scale
box_line_width = int(3.0*height_scale)
out_video = cv2.VideoWriter(OUT_FILE_PATH, fourcc, fps, (orig_width,orig_height))
frame_counter = 0
seconds = 0
# Read video frame by frame
while(True):
    # If not to use complete video
    if SHORTEN and seconds < START_SECOND:
        ret, frame = cap.read()
        frame_counter += 1
        if frame_counter == fps:
            seconds += 1
            print('Video second: %d/%d (skipped)' % (seconds,nr_seconds))
            frame_counter = 0
        continue
        
    if ret == True:
        # Read image frame
        frame = cv2.flip(frame,0)
        frame = cv2.flip(frame,1)
        img = cv2.resize(frame, dsize=(IMG_WIDTH, IMG_HEIGHT), interpolation=cv2.INTER_CUBIC)
        input_images = np.array([img])
        frame_counter += 1

        # Make predictions
        y_pred = model.predict(input_images)
        confidence_threshold = 0.6
        y_pred_thresh = [y_pred[k][y_pred[k,:,1] > confidence_threshold] for k in range(y_pred.shape[0])]
        
        for box in y_pred_thresh[0]:
            # Transform the predicted bounding boxes for the 300x300 image 
            # to the original image dimensions.
            xmin = int(box[2] * orig_width / IMG_WIDTH)
            ymin = int(box[3] * orig_height / IMG_HEIGHT)
            xmax = int(box[4] * orig_width / IMG_WIDTH)
            ymax = int(box[5] * orig_height / IMG_HEIGHT)
            # Draw it on image
            label = '{}: {:.2f}'.format(classes[int(box[0])], box[1])
            class_label = '{}'.format(classes[int(box[0])])
            text_box_width = int(float(len(label))*width_scale)
            color = colors[class_label]
            cv2.rectangle(frame, (xmin,ymin),(xmax,ymax), color, box_line_width)
            cv2.rectangle(frame, (xmin,ymin-text_box_height),(xmin+text_box_width, ymin), color, box_line_width)
            cv2.rectangle(frame, (xmin, ymin-text_box_height),(xmin+text_box_width, ymin), color,-1)
            cv2.putText(frame, label, (xmin, ymin+text_pos_height-text_box_height), font, fontScale=font_scale, color=(0, 0, 0), thickness=int(height_scale))

        # Write annotated frame to video
        out_video.write(frame)
        # Keep track of seconds
        if frame_counter == fps:
            seconds += 1
            print('Video second: %d/%d' % (seconds,nr_seconds))
            frame_counter = 0
        
    else:
        break
    if SHORTEN and seconds == NR_SECONDS:
        print('Stopped at second: %d/%d' % (seconds,nr_seconds))
        break
    ret, frame = cap.read()

cap.release()
out_video.release()




































