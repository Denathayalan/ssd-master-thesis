'''
Split between training and validation data
(can also be used to split training and test data)
Master thesis project Cybercom
Roger Kalliomaki
'''

import numpy as np

NR_OF_VAL_IMAGES = 5000

# Imageset paths
all_images_path = "./ImageSets/Main/train_val.txt"
train_images_file = open("./ImageSets/Main/train.txt", "w")
val_images_file = open("./ImageSets/Main/val.txt", "w")

# Read lines from file
with open(all_images_path) as f:
    content = f.readlines()
    
# Remove whitespace characters and create numpy array
#lines = np.array([x.strip() for x in content])

# Create numpy array
lines = np.array(content)

# Shuffle lines
np.random.shuffle(lines)

# Split in train val
train, val = lines[NR_OF_VAL_IMAGES:], lines[:NR_OF_VAL_IMAGES]

# Write to files
for line in train:
    train_images_file.write(line)
for line in val:
    val_images_file.write(line)

# Close files
train_images_file.close()
val_images_file.close()
