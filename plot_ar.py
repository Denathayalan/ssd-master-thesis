'''
Plot bounding box aspect ratios saved in given csv file
Master thesis project Cybercom
Roger Kalliomaki
'''

import matplotlib.pyplot as plt
import numpy as np

# Set bin factor (nr of samples in each bar), dataset and 
# the boolean USE_ALL_OBJECTS which defines if all objects
# should be used or only cars.
BIN_FACTOR = 5.0
DATASET = 'BDD100K'
USE_ALL_OBJECTS = True
NUM_BINS = 350

# Load csv file into numpy array
if USE_ALL_OBJECTS:
    FILE_PATH = './' + DATASET + '_all_classes_bb_aspect_ratios.csv'
else:
    FILE_PATH = './' + DATASET + '_cars_bb_aspect_ratios.csv'
aspect_ratios = np.genfromtxt(FILE_PATH, dtype=float)

# Plot histogram plot
fig, ax = plt.subplots()
n, bins, patches = ax.hist(aspect_ratios, NUM_BINS, range=(0,3.5))
ax.set_xlabel('Bounding Box Aspect Ratio')
ax.set_ylabel('Object Count')

# Save plot
if USE_ALL_OBJECTS:
    plt.savefig('all_classes_aspect_ratios_%s_num_bins_%d.png' % (DATASET,NUM_BINS))
else:
    plt.savefig('cars_aspect_ratios_%s_num_bins_%d.png' % (DATASET,NUM_BINS))

plt.show()
